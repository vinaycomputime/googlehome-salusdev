'use strict';

const AWS = require('aws-sdk');
var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const IOT_ENDPOINT = process.env.IOT_ENDPOINT;
const DEVICE_TABLE = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
var iot = new AWS.Iot();
var iotdata = new AWS.IotData({
    endpoint: IOT_ENDPOINT
});
var documentClient = new AWS.DynamoDB.DocumentClient();
var device_list;
var userId;
var thing;
var thingStr;
var device_array = {}
var errorResponse = 0;
var temperatureF;
var data;
var params;
var devId;
var thingGrp;
var reportstate;
var reportproperties;
var paramsThing;
var thngSearch;
var idx;
var deviceId;
var intent;
var syncData = 0;
var queryData = 0;
var execData = 0;
var payloadQ = {
    devices: {},
};
var payload;
//Make sure if const declared, It is also initialised otherwise throws error
var deviceE;

//20210507: REPORT_STATE
var tokenRS;
var agentUserIdRS;
var responseRS = {};
var reportStateData = 0;
var token;
var body;

//NEW:
const categoryFxn = require("./category");
var devname;
var dataQ;
var OnState;
var payloadE = {
    commands: [{
        ids: [],
        states: {}
    }]
};
var global_debug = 1;

exports.handler = async function(event, context) {

    //20210507: REPORT_STATE
    if ("body" in event) {
        body = JSON.parse(event.body);
        intent = body["inputs"][0]["intent"];
        token = event.headers.Authorization.split(" ")[1];
        console.log(token);
        console.log(event);
    } else {
        console.log("Event triggered by Lambda (specifically for ReportState)");
        console.log(event);
        console.log(context);
        intent = event.inputs[0]["intent"];
        console.log(event.agentUserId);
        console.log(event.token);
        console.log(event.intent);
        tokenRS = event.token;
        agentUserIdRS = event.agentUserId;
        intent = event.intent;
    }

    switch (intent) {
        case "action.devices.SYNC":
            console.log("SYNCING...");
            params = {
                "AccessToken": token
            };
            data = await cognitoidentityserviceprovider.getUser(params).promise();
            console.log(data);
            var user = data.Username;
            console.log(user);
            params = {
                TableName: DEVICE_TABLE,
                IndexName: "UserName-index",
                KeyConditionExpression: 'UserName = :username',
                ExpressionAttributeValues: {
                    ':username': user
                }
            }
            data = await documentClient.query(params).promise();
            console.log(data);
            device_list = JSON.parse(data.Items[0].Own);
            thing = device_list.list[0];
            device_array = device_list.list;

            if (global_debug == 1)
                device_array = ["SAU2AG1_GW-001E5E0082CA"];

            console.log(device_array.length);
            console.log(thing);
            userId = data.Items[0].userid;
            console.log(userId);
            payload = {
                agentUserId: userId,
                devices: []
            };

            var i;
            var len = device_array.length;
            for (i = 0; i < len; i++) {
                thingGrp = 'Gateway-' + device_array[i].split("-")[1];
                console.log(thingGrp);
                paramsThing = {
                    "thingGroupName": thingGrp,
                    "maxResults": 100
                };
                var dataS = await iot.listThingsInThingGroup(paramsThing).promise();
                console.log(dataS.things);
                for (var dev of dataS.things) {
                    console.log(dev);
                    var devType = dev.split('-');
                    if (devType.length > 2)
                        devType = devType[2]; //not applicable for solo thermostat
                    else
                        devType = devType[0]; //for solo thermostat

                    payload = await categoryFxn.handleDeviceSync(devType, dev, payload);
                }
            }

            syncData = 1;
            queryData = 0;
            execData = 0;
            reportStateData = 0;
            break;
        case "action.devices.QUERY":
            console.log("QUERYING...");
            devId = body.inputs[0].payload.devices[0].id
            console.log(devId);
            //read shadow and return response
            var devType = devId.split('-');
            if (devType.length > 2)
                devType = devType[2]; //not applicable for solo thermostat
            else
                devType = devType[0]; //for solo thermostat

            payloadQ = await categoryFxn.handleDeviceQuery(devType, devId, payloadQ);

            //app.requestSync('us-west-2:0c5f8041-55e7-4490-bcf7-d691ff6189be')
            //	.then((res) => { console.log('Request Sync Successful'); })
            //	.catch((res) => { console.log('Request Sync Failed', res); });

            syncData = 0;
            queryData = 1;
            execData = 0;
            reportStateData = 0;
            break;
        case "action.devices.EXECUTE":
            console.log("EXECUTING...");
            var cmdExe = body.inputs[0].payload.commands[0];
            console.log(body);
            console.log(cmdExe);
            console.log(cmdExe.execution[0]);
            devId = body.inputs[0].payload.commands[0].devices[0].id;
            console.log(devId);
            var desireproperties = {}
            var shadow;
            var systemmodeE;
            let desiredshadowState = {
                state: {
                    desired: {}
                }
            };

            var devType = devId.split('-');
            if (devType.length > 2)
                devType = devType[2]; //not applicable for solo thermostat
            else
                devType = devType[0]; //for solo thermostat

            payloadE = await categoryFxn.handleDeviceExecute(cmdExe, devType, devId, payloadE);

            //Read Shadow again before ReportState
            //wait for few seconds before shadow B read
            await categoryFxn.handleDeviceReportState(devType, devId, agentUserIdRS, body.requestId, tokenRS);

            syncData = 0;
            queryData = 0;
            execData = 1;
            reportStateData = 0;

            break;
        default:
            console.log("Intent in event not found");
    }

    if (syncData == 1) {
        return {
            isBase64Encoded: false,
            statusCode: 200,
            body: JSON.stringify({
                requestId: body.requestId,
                payload: payload
            }),
        };
        console.log(payload);
    } else if (queryData == 1) {
        return {
            isBase64Encoded: false,
            statusCode: 200,
            body: JSON.stringify({
                requestId: body.requestId,
                payload: payloadQ
            }),
        };
    } else if (execData == 1) {
        if (errorResponse == 1) {
            return {
                isBase64Encoded: false,
                statusCode: 200,
                body: JSON.stringify({
                    requestId: body.requestId,
                    payload: {
                        commands: [{
                            ids: [devId],
                            status: 'ERROR',
                            errorCode: "inOffMode"
                        }]
                    }
                }),
            };
        } else {
            return {
                isBase64Encoded: false,
                statusCode: 200,
                body: JSON.stringify({
                    requestId: body.requestId,
                    payload: payloadE
                }),
            };
        }
    }
};


async function getModelDescr(devType) {
    var modelId = devType;

    var params = {
        TableName: MODEL_TABLE_NAME,
        KeyConditionExpression: 'ModelID = :ModelID',
        ExpressionAttributeValues: {
            ':ModelID': modelId
        }
    }
    var modeldesc = await documentClient.query(params).promise();
    return modeldesc;
}