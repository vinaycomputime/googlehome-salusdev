'use strict';

const AWS = require('aws-sdk');
const IOT_ENDPOINT = process.env.IOT_ENDPOINT;
const DEVICE_TABLE = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
var iot = new AWS.Iot();
var iotdata = new AWS.IotData({
    endpoint: IOT_ENDPOINT
});
var documentClient = new AWS.DynamoDB.DocumentClient();
var data;
var params;

//NEW:
var devname;
var OnState;
var shadowQ;

async function handleSync(devType, dev, payload) {
    var modelDes = await getModelDescr(devType);
    console.log(modelDes.Items[0]);
    devname = await getDevNameFromShadow(modelDes, dev);
    var device = dev;

    payload.devices.push({
        id: device,
        type: modelDes.Items[0].DeviceType,
        traits: modelDes.Items[0].Traits,
        name: {
            defaultNames: [devname],
            name: devname,
            nicknames: [devname]
        },
        willReportState: true,
        attributes: modelDes.Items[0].Attributes,
        deviceInfo: {
            manufacturer: 'Computime',
            model: 'SAU2AG1_GW',
            hwVersion: '1.1',
            swVersion: '2.1'
        }
    });

    return payload;
}

async function handleQuery(devType, devId, payloadQ) {
    var modelDes = await getModelDescr(devType);
    var nodeQ = modelDes.Items[0].nodeID;
    var paramsQ = {
        "thingName": devId
    };
    var dataQ = await iotdata.getThingShadow(paramsQ).promise();
    var shadowQ = JSON.parse(dataQ.payload);
    console.log(shadowQ);
    var reportstateSP = shadowQ.state.reported;
    var reportpropertiesSP = reportstateSP[nodeQ].properties;
    var propertiesprefixSP = modelDes.Items[0].NodeEndpoint;
    var onoff = reportpropertiesSP[propertiesprefixSP + ":sOnOffS:OnOff"];
    var onlevel = reportpropertiesSP[propertiesprefixSP + ":sLevelS:OnLevel"];
    var currLvl = reportpropertiesSP[propertiesprefixSP + ":sLevelS:CurrentLevel"];

    if (onoff == 1)
        onoff = true;
    else
        onoff = false;

    payloadQ.devices[devId] = {
        online: true,
        on: onoff,
        brightness: currLvl,
        color: {
            "temperatureK": 3000
        },
        status: 'SUCCESS'
    };

    return payloadQ;
}

async function handleExecute(cmdExe, devType, devId, payloadE) {
    var modelDes = await getModelDescr(devType);
    var nodeE = modelDes.Items[0].nodeID;
    var propertiesprefixE = modelDes.Items[0].NodeEndpoint;
    var desireproperties = {}
    let desiredshadowState = {
        state: {
            desired: {}
        }
    };
    var brightnessLevel;
    var colorName;
    var colorTemp;

    if (cmdExe.execution[0].command == "action.devices.commands.OnOff") {
        if (cmdExe.execution[0].params.on == true) {
            OnState = true;
            //update shadow
            desireproperties[propertiesprefixE + ":sOnOffS:SetOnOff"] = 1;
        } else {
            OnState = false;
            desireproperties[propertiesprefixE + ":sOnOffS:SetOnOff"] = 0;
        }
        desiredshadowState.state.desired[nodeE] = {
            properties: desireproperties
        };
        await updateThingShadow(devId, desiredshadowState);
    } else if (cmdExe.execution[0].command == "action.devices.commands.BrightnessAbsolute") {
        brightnessLevel = cmdExe.execution[0].params.brightness;
        desireproperties[propertiesprefixE + ":sLevelS:SetOnLevel"] = brightnessLevel;
        desireproperties[propertiesprefixE + ":sLevelS:SetCurrentLevel"] = brightnessLevel;
        desiredshadowState.state.desired[nodeE] = {
            properties: desireproperties
        };
        await updateThingShadow(devId, desiredshadowState);
    } else if (cmdExe.execution[0].command == "action.devices.commands.ColorAbsolute") {
        colorName = cmdExe.execution[0].params.color.name;
        colorTemp = cmdExe.execution[0].params.color.temperature;
        //need to update color to device
    }

    payloadE.commands[0].ids = [devId];
    payloadE.commands[0].states = {
        online: true,
        on: OnState,
        brightness: brightnessLevel,
        color: {
            temperatureK: colorTemp
        }
    };

    return payloadE;
}

async function getModelDescr(devType) {
    var modelId = devType;

    var params = {
        TableName: MODEL_TABLE_NAME,
        KeyConditionExpression: 'ModelID = :ModelID',
        ExpressionAttributeValues: {
            ':ModelID': modelId
        }
    }
    var modeldesc = await documentClient.query(params).promise();
    return modeldesc;
}

async function getDevNameFromShadow(modelDes, dev) {
    devname = modelDes.Items[0].deviceName;
    params = {
        "thingName": dev
    };
    var dataShd = await iotdata.getThingShadow(params).promise();
    var shadowS = JSON.parse(dataShd.payload);
    console.log(shadowS);
    var reportstateS = shadowS.state.reported;
    var reportpropertiesS = reportstateS[modelDes.Items[0].nodeID].properties;
    var propertiesprefixS = modelDes.Items[0].NodeEndpoint;
    devname = reportpropertiesS[propertiesprefixS + devname];
    devname = JSON.parse(devname).deviceName; //Not applicable for Solo thermostats
    return devname;
}

async function updateThingShadow(thingName, shadowState) {
    try {
        var payloadjson = JSON.stringify(shadowState)
        console.log(payloadjson) //vinay
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        var data = await iotdata.publish(params).promise();
        console.log(data);
    } catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

module.exports = {
    handleSync,
    handleQuery,
    handleExecute
};