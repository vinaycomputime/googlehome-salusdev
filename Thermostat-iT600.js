'use strict';

const AWS = require('aws-sdk');
var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const IOT_ENDPOINT = process.env.IOT_ENDPOINT;
const DEVICE_TABLE = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
var iot = new AWS.Iot();
var iotdata = new AWS.IotData({
    endpoint: IOT_ENDPOINT
});
var documentClient = new AWS.DynamoDB.DocumentClient();
var reqId;
var tempSetpt = 20.0;
var sysMode = 'heat';
var temperature = 25.0;
var errorResponse = 0;
var temperatureF;
var data;
var params;
var devId;
var reportstate;
var reportproperties;
var payloadQ = {
    devices: {},
};
var payload;
//Make sure if const declared, It is also initialised otherwise throws error
var deviceE;

//NEW:
var devname;
var dataQ;
var payloadE = {
    commands: [{
        ids: [],
        states: {},
        status: 'SUCCESS'
    }]
};
const RSFxn = require("./ReportState");

async function handleSync(devType, dev, payload) {
    var modelDes = await getModelDescr(devType);
    console.log(modelDes.Items[0]);
    devname = await getDevNameFromShadow(modelDes, dev);
    var device = dev;

    payload.devices.push({
        id: device,
        type: modelDes.Items[0].DeviceType,
        traits: modelDes.Items[0].Traits,
        name: {
            defaultNames: [devname],
            name: devname,
            nicknames: [devname]
        },
        willReportState: true,
        attributes: modelDes.Items[0].Attributes,
        deviceInfo: {
            manufacturer: 'Computime',
            model: 'SAU2AG1_GW',
            hwVersion: '1.1',
            swVersion: '2.1'
        }
    });

    return payload;
}

async function handleQuery(devType, devId, payloadQ) {
    var modelDes = await getModelDescr(devType);
    console.log(modelDes.Items[0]);
    var nodeQ = modelDes.Items[0].nodeID;
    var paramsQ = {
        "thingName": devId
    };
    var dataQ = await iotdata.getThingShadow(paramsQ).promise();
    var shadowQ = JSON.parse(dataQ.payload);
    console.log(shadowQ);
    var reportstate = shadowQ.state.reported;
    var reportproperties = reportstate[nodeQ].properties;
    var propertiesprefix = modelDes.Items[0].NodeEndpoint;
    temperature = reportproperties[propertiesprefix + ":sIT600TH:LocalTemperature_x100"];
    temperature = temperature / 100;
    console.log(temperature);

    var heatingsetpointx100 = reportproperties[propertiesprefix + ":sIT600TH:HeatingSetpoint_x100"];
    var heatingsetpoint = heatingsetpointx100 / 100;
    console.log(heatingsetpoint);
    var coolingsetpoint = reportproperties[propertiesprefix + ":sIT600TH:HeatingSetpoint_x100"];
    coolingsetpoint = coolingsetpoint / 100;
    console.log(coolingsetpoint);
    var systemmode = reportproperties[propertiesprefix + ":sIT600TH:SystemMode"];
    console.log(systemmode);
    var runningmode = reportproperties[propertiesprefix + ":sIT600TH:RunningMode"];
    console.log(runningmode);
    switch (systemmode) {
        case 0:
            sysMode = 'off';
            break;
        case 1:
            if (runningmode === 3) {
                sysMode = 'cool';
                tempSetpt = coolingsetpoint;
            } else if (runningmode === 4) {
                sysMode = 'heat';
                tempSetpt = heatingsetpoint;
            }
            break;
        case 3:
            sysMode = 'cool';
            tempSetpt = coolingsetpoint;
            break;
        case 4:
            sysMode = 'heat';
            tempSetpt = heatingsetpoint;
            break;
    }

    payloadQ.devices[devId] = {
        online: true,
        thermostatMode: sysMode,
        thermostatTemperatureSetpoint: tempSetpt,
        thermostatTemperatureAmbient: temperature,
        status: 'SUCCESS'
    };

    return payloadQ;
}

async function handleExecute(cmdExe, devType, devId, payloadE) {
    var modelDes = await getModelDescr(devType);
    console.log(modelDes.Items[0]);
    var nodeid = modelDes.Items[0].nodeID;
    var propertiesprefix = modelDes.Items[0].NodeEndpoint;
    var desireproperties = {}
    let desiredshadowState = {
        state: {
            desired: {}
        }
    };

    if (cmdExe.execution[0].params.thermostatTemperatureSetpoint) {
        console.log('Received thermostatTemperatureSetpoint....');
        tempSetpt = cmdExe.execution[0].params.thermostatTemperatureSetpoint;
        console.log(tempSetpt);
        tempSetpt = Math.round(tempSetpt);
        console.log(tempSetpt);

        var paramsE = {
            "thingName": devId
        };
        var dataE = await iotdata.getThingShadow(paramsE).promise();
        var shadow = JSON.parse(dataE.payload);
        reportstate = shadow.state.reported;
        reportproperties = reportstate[nodeid].properties;
        var systemmodeE = reportproperties[propertiesprefix + ":sIT600TH:SystemMode"];
        if (systemmodeE == 0)
            errorResponse = 1;

        temperature = reportproperties[propertiesprefix + ":sIT600TH:LocalTemperature_x100"];
        temperature = temperature / 100;
        var runingmod = reportproperties[propertiesprefix + ":sIT600TH:RunningMode"];
        switch (systemmodeE) {
            case 3:
                errorResponse = 0;
                desireproperties[propertiesprefix + ":sIT600TH:SetHeatingSetpoint_x100"] = tempSetpt * 100;
                desiredshadowState.state.desired[nodeid] = {
                    properties: desireproperties
                };
                await updateThingShadow(devId, desiredshadowState);
                console.log('SetHeatingSetpoint_x100');
                break
            case 4:
                errorResponse = 0;
                desireproperties[propertiesprefix + ":sIT600TH:SetHeatingSetpoint_x100"] = tempSetpt * 100;
                desiredshadowState.state.desired[nodeid] = {
                    properties: desireproperties
                };
                await updateThingShadow(devId, desiredshadowState);
                break;
            case 0:
                errorResponse = 1;
                //20210512: For TestSuite run
                //errorResponse = 0;
                //desireproperties[propertiesprefix + ":sIT600TH:SetHeatingSetpoint_x100] = tempSetpt*100
                //desiredshadowState.state.desired[nodeid] = {properties: desireproperties};
                //await updateThingShadow(devId, desiredshadowState);
                break;
                //handle case 1 for auto based on running mode	
            case 1:
                errorResponse = 0;
                if (runingmod === 3) {
                    desireproperties[propertiesprefix + ":sIT600TH:SetHeatingSetpoint_x100"] = tempSetpt * 100;
                    desiredshadowState.state.desired[nodeid] = {
                        properties: desireproperties
                    };
                    await updateThingShadow(devId, desiredshadowState);
                } else
                if (runingmod === 4) {
                    desireproperties[propertiesprefix + ":sIT600TH:SetHeatingSetpoint_x100"] = tempSetpt * 100;
                    desiredshadowState.state.desired[nodeid] = {
                        properties: desireproperties
                    };
                    await updateThingShadow(devId, desiredshadowState);
                }
                break;
        }
    } else if (cmdExe.execution[0].params.thermostatMode) {
        sysMode = cmdExe.execution[0].params.thermostatMode;
        errorResponse = 0;
        if (sysMode == 'cool') {
            desireproperties[propertiesprefix + ":sIT600TH:SetSystemMode"] = 3;
            desireproperties[propertiesprefix + ":sIT600TH:SetFanMode"] = 5;
            desiredshadowState.state.desired[nodeid] = {
                properties: desireproperties
            };
            await updateThingShadow(devId, desiredshadowState);
        } else if (sysMode == 'heat') {
            desireproperties[propertiesprefix + ":sIT600TH:SetSystemMode"] = 4;
            desireproperties[propertiesprefix + ":sIT600TH:SetFanMode"] = 5;
            desiredshadowState.state.desired[nodeid] = {
                properties: desireproperties
            };
            await updateThingShadow(devId, desiredshadowState);
        } else if (sysMode == 'off') {
            desireproperties[propertiesprefix + ":sIT600TH:SetSystemMode"] = 0
            desiredshadowState.state.desired[nodeid] = {
                properties: desireproperties
            };
            await updateThingShadow(devId, desiredshadowState);
        } else if (sysMode == 'auto') {
            desireproperties[propertiesprefix + ":sIT600TH:SetSystemMode"] = 1
            //desireproperties["ep0:sPTAC868:SetFanMode"] = 5;
            desiredshadowState.state.desired[nodeid] = {
                properties: desireproperties
            };
            await updateThingShadow(devId, desiredshadowState);
        }
    }

    //Wait for 3 secs before shadow read again for ReportState fxn call, Refer Ptac

	if(cmdExe.execution[0].hasOwnProperty('challenge'))
	{	
		payloadE.commands[0].ids = [devId];
		payloadE.commands[0].states = {
			online: true,
			thermostatMode: sysMode,
			thermostatTemperatureSetpoint: tempSetpt,
			thermostatTemperatureAmbient: temperature
		};
		payloadE.commands[0].status = 'SUCCESS';
	}
	else
	{
		payloadE.commands[0].ids = [devId];
		payloadE.commands[0].states = {
			online: true,
			thermostatMode: sysMode,
			thermostatTemperatureSetpoint: tempSetpt,
			thermostatTemperatureAmbient: temperature
		};
		payloadE.commands[0].status = 'ERROR';	
		payloadE.commands[0].errorCode = 'challengeNeeded';
		payloadE.commands[0].challengeNeeded = {
									type: "ackNeeded"
								};
	}
	
    return payloadE;
}

async function handleReportState(devType, devId, agentUserIdRS, requestId, tokenRS) {
    //Read Shadow again before ReportState
    //wait for few seconds before shadow B read
    await new Promise(r => setTimeout(r, 3000));
    console.log('calling ReportState....');

    var modelDesRS = await getModelDescr(devType);
    var nodeidRS = modelDesRS.Items[0].nodeID;
    var propertiesprefixRS = modelDesRS.Items[0].NodeEndpoint;

    var paramsRS = {
        "thingName": devId
    };
    var dataRS = await iotdata.getThingShadow(paramsRS).promise();
    var shadow = JSON.parse(dataRS.payload);
    console.log(shadow);
    reportstate = shadow.state.reported;
    reportproperties = reportstate[nodeidRS].properties;
    temperature = reportproperties[propertiesprefixRS + ":sIT600TH:LocalTemperature_x100"];
    temperature = temperature / 100;
    var heatingsetpointRS = reportproperties[propertiesprefixRS + ":sIT600TH:HeatingSetpoint_x100"];
    heatingsetpointRS = heatingsetpointRS / 100;
    var coolingsetpointRS = reportproperties[propertiesprefixRS + ":sIT600TH:HeatingSetpoint_x100"];
    coolingsetpointRS = coolingsetpointRS / 100;
    var systemmodeRS = reportproperties[propertiesprefixRS + ":sIT600TH:SystemMode"];
    var runningmodeRS = reportproperties[propertiesprefixRS + ":sIT600TH:RunningMode"];
    switch (systemmodeRS) {
        case 0:
            sysMode = 'off';
            break;
        case 1:
            if (runningmodeRS === 3) {
                sysMode = 'cool';
                tempSetpt = coolingsetpointRS;
            } else if (runningmodeRS === 4) {
                sysMode = 'heat';
                tempSetpt = heatingsetpointRS;
            }
            break;
        case 3:
            sysMode = 'cool';
            tempSetpt = coolingsetpointRS;
            break;
        case 4:
            sysMode = 'heat';
            tempSetpt = heatingsetpointRS;
            break;
    }

    //device name: VR00ZN000786479
    var device9DE = "SAU2AG1_GW-001E5E0082CA-SQ610RF-001E5E09025339DE";

    var payloadRS = {
        devices: {
            states: {}
        }
    };
    payloadRS.devices.states[device9DE] = {
        online: true,
        thermostatMode: sysMode,
        thermostatTemperatureSetpoint: tempSetpt,
        thermostatTemperatureAmbient: temperature
    }

    await RSFxn.callReportState(agentUserIdRS, requestId, payloadRS, tokenRS);
}

async function getModelDescr(devType) {
    var modelId = devType;

    var params = {
        TableName: MODEL_TABLE_NAME,
        KeyConditionExpression: 'ModelID = :ModelID',
        ExpressionAttributeValues: {
            ':ModelID': modelId
        }
    }
    var modeldesc = await documentClient.query(params).promise();
    return modeldesc;
}

async function getDevNameFromShadow(modelDes, dev) {
    devname = modelDes.Items[0].deviceName;
    params = {
        "thingName": dev
    };
    var dataShd = await iotdata.getThingShadow(params).promise();
    var shadowS = JSON.parse(dataShd.payload);
    console.log(shadowS);
    var reportstateS = shadowS.state.reported;
    var reportpropertiesS = reportstateS[modelDes.Items[0].nodeID].properties;
    var propertiesprefixS = modelDes.Items[0].NodeEndpoint;
    devname = reportpropertiesS[propertiesprefixS + devname];
    devname = JSON.parse(devname).deviceName; //Not applicable for Solo thermostats
    return devname;
}

async function updateThingShadow(thingName, shadowState) {
    try {
        var payloadjson = JSON.stringify(shadowState)
        console.log(payloadjson) //vinay
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        var data = await iotdata.publish(params).promise();
        console.log(data);
    } catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

async function getModelDescr(devType) {
    var modelId = devType;

    var params = {
        TableName: MODEL_TABLE_NAME,
        KeyConditionExpression: 'ModelID = :ModelID',
        ExpressionAttributeValues: {
            ':ModelID': modelId
        }
    }
    var modeldesc = await documentClient.query(params).promise();
    return modeldesc;
}

async function getDevNameFromShadow(modelDes, dev) {
    devname = modelDes.Items[0].deviceName;
    params = {
        "thingName": dev
    };
    var dataShd = await iotdata.getThingShadow(params).promise();
    var shadowS = JSON.parse(dataShd.payload);
    console.log(shadowS);
    var reportstateS = shadowS.state.reported;
    var reportpropertiesS = reportstateS[modelDes.Items[0].nodeID].properties;
    var propertiesprefixS = modelDes.Items[0].NodeEndpoint;
    devname = reportpropertiesS[propertiesprefixS + devname];
    devname = JSON.parse(devname).deviceName; //Not applicable for Solo thermostats
    return devname;
}

async function getDeviceShadowReport(modelDes, dev) {
    params = {
        "thingName": dev
    };
    var dataShd = await iotdata.getThingShadow(params).promise();
    var shadowS = JSON.parse(dataShd.payload);
    console.log(shadowS);
    var reportstateS = shadowS.state.reported;
    return reportstateS;
}

async function updateThingShadow(thingName, shadowState) {
    try {
        var payloadjson = JSON.stringify(shadowState)
        console.log(payloadjson) //vinay
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        var data = await iotdata.publish(params).promise();
        console.log(data);
    } catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

function convertTemperature(temperature, currentScale, desiredScale) {
    if (currentScale === desiredScale) {
        return temperature;
    } else if (currentScale === 'FAHRENHEIT' && desiredScale === 'CELSIUS') {
        return convertFahrenheitToCelsius(temperature);
    } else if (currentScale === 'CELSIUS' && desiredScale === 'FAHRENHEIT') {
        return convertCelsiusToFahrenheit(temperature);
    } else {
        throw new Error(`Unable to convert ${currentScale} to ${desiredScale}, unsupported temp scale.`);
    }
}

function convertCelsiusToFahrenheit(celsius) {
    var fahrenheit = Math.round(((celsius * 1.8) + 32) * 100);
    return Math.round(fahrenheit / 100);
}

function convertFahrenheitToCelsius(fahrenheit) {
    var celsius = Math.round(((fahrenheit - 32) * 0.556) * 100);
    return celsius / 100;
}

module.exports = {
    handleSync,
    handleQuery,
    handleExecute,
    handleReportState
};