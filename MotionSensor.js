'use strict';

const AWS = require('aws-sdk');
const IOT_ENDPOINT = process.env.IOT_ENDPOINT;
const DEVICE_TABLE = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
var iot = new AWS.Iot();
var iotdata = new AWS.IotData({
    endpoint: IOT_ENDPOINT
});
var documentClient = new AWS.DynamoDB.DocumentClient();
var data;
var params;

//NEW:
var devname;
var OnState;
var shadowQ;

async function handleSync(devType, dev, payload) {
    var modelDes = await getModelDescr(devType);
    console.log(modelDes.Items[0]);
    devname = await getDevNameFromShadow(modelDes, dev);
    var device = dev;

    payload.devices.push({
        id: device,
        type: modelDes.Items[0].DeviceType,
        traits: modelDes.Items[0].Traits,
        name: {
            defaultNames: [devname],
            name: devname,
            nicknames: [devname]
        },
        willReportState: true,
        attributes: modelDes.Items[0].Attributes,
        deviceInfo: {
            manufacturer: 'Computime',
            model: 'SAU2AG1_GW',
            hwVersion: '1.1',
            swVersion: '2.1'
        }
    });

    return payload;
}

async function handleQuery(devType, devId, payloadQ) {
    var modelDes = await getModelDescr(devType);
    var nodeQ = modelDes.Items[0].nodeID;
    var paramsQ = {
        "thingName": devId
    };
    var dataQ = await iotdata.getThingShadow(paramsQ).promise();
    var shadowQ = JSON.parse(dataQ.payload);
    console.log(shadowQ);
    var reportstateSP = shadowQ.state.reported;
    var reportpropertiesSP = reportstateSP[nodeQ].properties;
    var propertiesprefixSP = modelDes.Items[0].NodeEndpoint;
    var zonestate = reportpropertiesSP[propertiesprefixSP + "sIASZS:ZoneState_d"];
    var batteryRemain = reportpropertiesSP[propertiesprefixSP + "sPowerS:BatteryRemaining"];

    if (zonestate == 1)
        zonestate = "healthy";
    else
        zonestate = "unhealthy";

    payloadQ.devices[devId] = {
        online: true,
        currentSensorStateData: [{
            name: "AirQuality",
            currentSensorState: zonestate
        }],
        descriptiveCapacityRemaining: "HIGH",
        capacityRemaining: [{
            unit: "PERCENTAGE",
            rawValue: 90
        }],
        status: 'SUCCESS'
    };

    return payloadQ;
}


async function getModelDescr(devType) {
    var modelId = devType;

    var params = {
        TableName: MODEL_TABLE_NAME,
        KeyConditionExpression: 'ModelID = :ModelID',
        ExpressionAttributeValues: {
            ':ModelID': modelId
        }
    }
    var modeldesc = await documentClient.query(params).promise();
    return modeldesc;
}

async function getDevNameFromShadow(modelDes, dev) {
    devname = modelDes.Items[0].deviceName;
    params = {
        "thingName": dev
    };
    var dataShd = await iotdata.getThingShadow(params).promise();
    var shadowS = JSON.parse(dataShd.payload);
    console.log(shadowS);
    var reportstateS = shadowS.state.reported;
    var reportpropertiesS = reportstateS[modelDes.Items[0].nodeID].properties;
    var propertiesprefixS = modelDes.Items[0].NodeEndpoint;
    devname = reportpropertiesS[propertiesprefixS + devname];
    devname = JSON.parse(devname).deviceName; //Not applicable for Solo thermostats
    return devname;
}


module.exports = {
    handleSync,
    handleQuery
};