'use strict';

const {smarthome} = require('actions-on-google');
const AWS = require('aws-sdk');
var payload;

const app = smarthome({
    jwt: require('./computimedev-04f74993c6a1.json')
});

async function callReportState(agentUserId, requestId, payloadRS, token) {
    agentUserId = 'us-west-2:0c5f8041-55e7-4490-bcf7-d691ff6189be';
    var request = {
        requestId: requestId,
        agentUserId: agentUserId,
        payload: payloadRS
    };

    console.log(JSON.stringify(request));
    // Call reportState via smarthome app
    app.reportState(request)
        .then(() => {
            console.log("Report State Successful.");
        })
        .catch(error => {
            console.log("Report State ERROR");
            console.log(error.stack);
        });
    await new Promise(r => setTimeout(r, 3000));

    return request;
}

module.exports = {
    callReportState
};