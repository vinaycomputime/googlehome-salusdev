'use strict';

const AWS = require('aws-sdk');
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
var documentClient = new AWS.DynamoDB.DocumentClient();

var payloadQ = {
    devices: {},
};
var payload;
var payloadE = {
    commands: [{
        ids: [],
        states: {},
        status: 'SUCCESS'
    }]
};
var modelDes;

//DO NOT use 'async' function for normal operation 
function IsIT600Thermostat(devType) {
    if (devType == 'it600ThermHW' || devType == 'SQ610RF' || devType == 'SQ610') {
        return true;
    } else {
        return false;
    }
}

function IsSmartPlug(devType) {
    if (devType == 'SP600' || devType == 'SPE600') {
        return true;
    } else {
        return false;
    }
}

function IsSoloThermostat(devType) {
    if (devType == 'ST920WF' || devType == 'ST921WF') {
        return true;
    } else {
        return false;
    }
}

function IsSmartLight(devType) {
    if (devType == 'PAR16_RGBW_Z3' || devType == 'FLEX_RGBW_Z3') {
        return true;
    } else {
        return false;
    }
}

function IsSensor(devType) {
    if (devType == 'MS600') {
        return true;
    } else {
        return false;
    }
}

async function handleCategorySync(modelCategory, devType, dev, payload) {
    var CtgLib = modelCategory.Items[0].categoryLib;
    return (await require(CtgLib).handleSync(devType, dev, payload));
}

async function handleCategoryQuery(modelCategory, devType, devId, payloadQ) {
    var CtgLib = modelCategory.Items[0].categoryLib;
    return (await require(CtgLib).handleQuery(devType, devId, payloadQ));
}

async function handleCategoryExecute(cmdExe, modelCategory, devType, devId, payloadE) {
    var CtgLib = modelCategory.Items[0].categoryLib;
    return (await require(CtgLib).handleExecute(cmdExe, devType, devId, payloadE));
}

async function handleDeviceSync(devType, dev, payload) {
    if (IsSmartPlug(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payload = await handleCategorySync(modelDes, devType, dev, payload);
    }
    if (IsIT600Thermostat(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payload = await handleCategorySync(modelDes, devType, dev, payload);
    }
    if (IsSmartLight(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payload = await handleCategorySync(modelDes, devType, dev, payload);
    }
    if (IsSensor(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payload = await handleCategorySync(modelDes, devType, dev, payload);
    }
    return payload;
}

async function handleDeviceQuery(devType, devId, payloadQ) {
    if (IsSmartPlug(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadQ = await handleCategoryQuery(modelDes, devType, devId, payloadQ);
    }
    if (IsIT600Thermostat(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadQ = await handleCategoryQuery(modelDes, devType, devId, payloadQ);
    }
    if (IsSmartLight(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadQ = await handleCategoryQuery(modelDes, devType, devId, payloadQ);
    }
    if (IsSensor(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadQ = await handleCategoryQuery(modelDes, devType, devId, payloadQ);
    }
    return payloadQ;
}

async function handleDeviceExecute(cmdExe, devType, devId, payloadE) {
    if (IsSmartPlug(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadE = await handleCategoryExecute(cmdExe, modelDes, devType, devId, payloadE);
    }
    if (IsIT600Thermostat(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadE = await handleCategoryExecute(cmdExe, modelDes, devType, devId, payloadE);
    }
    if (IsSmartLight(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadE = await handleCategoryExecute(cmdExe, modelDes, devType, devId, payloadE);
    }
    if (IsSensor(devType) == true) {
        modelDes = await getModelDescr(devType);
        console.log(modelDes.Items[0]);
        payloadE = await handleCategoryExecute(cmdExe, modelDes, devType, devId, payloadE);
    }
    return payloadE;
}

async function handleDeviceReportState(devType, devId, agentUserIdRS, requestId, tokenRS) {
    //Read Shadow again before ReportState
    //wait for few seconds before shadow B read

    if (IsIT600Thermostat(devType) == true) {
        modelDes = await getModelDescr(devType);
        var CtgLib = modelDes.Items[0].categoryLib;
        await require(CtgLib).handleReportState(devType, devId, agentUserIdRS, requestId, tokenRS);
    }
}

async function getModelDescr(devType) {
    var modelId = devType;

    var params = {
        TableName: MODEL_TABLE_NAME,
        KeyConditionExpression: 'ModelID = :ModelID',
        ExpressionAttributeValues: {
            ':ModelID': modelId
        }
    }
    var modeldesc = await documentClient.query(params).promise();
    return modeldesc;
}

module.exports = {
    handleDeviceSync,
    handleDeviceQuery,
    handleDeviceExecute,
    handleDeviceReportState
};